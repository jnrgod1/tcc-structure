const User = require('../models/User')
const Project = require('../models/Project')

module.exports = {
  async index(req, res) {
    const { user_id } = req.params

    const user = await User.findByPk(user_id, {
      include: { association: 'projects' },
    })

    return res.json(user)
  },

  async store(req, res) {
    const { user_id } = req.params
    const { name } = req.body

    const user = await User.findByPk(user_id)

    if (!user) {
      res.status(400).json({ error: 'user not found' })
    }

    const project = await Project.create({
      name,
      user_id,
    })

    return res.json(project)
  },
}
