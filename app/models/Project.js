const { Model, DataTypes } = require('sequelize')

class Project extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
      },
      {
        sequelize,
      },
    )
  }
  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id', as: 'maintainer' })
  }
}

module.exports = Project
