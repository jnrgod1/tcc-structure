const express = require('express')
const UserController = require('../controllers/UserController')
const ProjectController = require('../controllers/ProjectController')

const routes = express.Router()

// USER
routes.get('/users', UserController.index)
routes.post('/user-create', UserController.store)

// PROJECTS
routes.get('/user/:user_id/projects', ProjectController.index)
routes.post('/user/:user_id/create-project', ProjectController.store)

module.exports = routes
