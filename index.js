const express = require('express')
const app = express()
const port = 8001
const routes = require('./app/routes/routes')

require('./app/database')

app.use(express.json())

app.use(routes)

app.listen(port, () => {
  console.log('Server running in port ' + port)
})
